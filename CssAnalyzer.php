<?php

/**
 * uwc5 CssAnalyzer
 */
class CssAnalyzer {
    /**
     *
     * @var string
     */
    protected $_url;
    
    /**
     *
     * @var string
     */
    protected $_scheme;
    
    /**
     *
     * @var string
     */
    protected $_host;
    
    /**
     *
     * @var string
     */
    protected $_path;
    
    /**
     *
     * @var array
     */
    protected $_paths = array();
    
    /**
     *
     * @var array
     */
    protected $_walkedPaths = array();
    
    /**
     *
     * @var array
     */
    protected $_cssFiles = array();
    
    /**
     *
     * @var array
     */
    protected $_uniques = array();
    
    /**
     *
     * @var array
     */
    protected $_depthExceededSelectors = array();
    
    /**
     *
     * @var boolean
     */
    protected $_scanAllSite;
    
    /**
     *
     * @var integer
     */
    protected $_maxLevel = 0;
    
    /**
     *
     * @var integer
     */
    protected $_maxPageCount = 0;
    
    /**
     *
     * @var float
     */
    protected $_timeForLoad = 0;
    
    /**
     * 
     * @param string $url
     * @param boolean $scanAllSite
     * @param integer $maxLevel
     * @param integer $maxPageCount
     * @throws Exception
     */
    public function __construct($url, $scanAllSite = false, $maxLevel = 0, $maxPageCount = 0) {
        if(!$url) {
            throw new Exception('url cannot be empty');
        }
        
        $this->_url = $url;
        
        $result = parse_url($url);
        $this->_scheme = isset($result['scheme']) ? $result['scheme'] : 'http';
        $this->_host = isset($result['host']) ? $result['host'] : null;
        $this->_path = isset($result['path']) ? $result['path'] : '/';
        
        array_push($this->_paths, $this->_path);
        
        $this->_scanAllSite = $scanAllSite;
        $this->_maxLevel = $maxLevel;
        $this->_maxPageCount = $maxPageCount;
    }
    
    /**
     * 
     * @return float
     */
    public function getTimeForLoad() {
        return $this->_timeForLoad;
    }
    
    /**
     * 
     * @param string $string
     * @return array
     */
    protected function _breakCss($string) {
        preg_match_all('/(.+?)\s?\{\s?.+?\s?\}/', $string, $matches);
        return $matches[1];
    }
    
    /**
     * 
     * @param string $file
     * @return boolean
     */
    protected function _fillUniques($file) {
        $tic = microtime(true);
        $string = file_get_contents($file);
        file_put_contents('/tmp/' . md5($file), $string);
        $tac = microtime(true);
        $this->_timeForLoad += ($tac - $tic);
        
        foreach($this->_breakCss($string) as $selector) {
            $selector = trim($selector);
            if(!$selector) {
                continue;
            }

            if(false !== strpos($selector, ',')) {
                foreach(explode(',', $selector) as $part) {
                    $part = trim($part);
                    if(false !== strpos($part, ':')) {
                        $part = substr($part, 0, strpos($part, ':'));
                        if(!in_array($part, $this->_uniques)) {
                            array_push($this->_uniques, $part);
                        }
                    }
                }
            }

            if(false !== strpos($selector, ':')) {
                $selector = substr($selector, 0, strpos($selector, ':'));
                if(!in_array($selector, $this->_uniques)) {
                    array_push($this->_uniques, $selector);
                }
            }
        }
        
        return true;
    }
    
    /**
     * 
     * @param DOMNode $node
     * @return boolean
     */
    protected function _fillLinks($node) {
        if('a' == $node->nodeName) {
            $href = $node->attributes->getNamedItem('href');
            if($href) {
                $result = parse_url($href->value);
                if((isset($result['host']) && $result['host'] == $this->_host) ||
                        (!isset($result['host']) && !isset($result['scheme']) && isset($result['path']))) {
                    $path = isset($result['path']) ? $result['path'] : '/';
                    if(!in_array($path, $this->_paths)) {
                        array_push($this->_paths, $path);
                    }
                }
            }
        }
        
        if(!$node->childNodes) {
            return false;
        }
        
        for($i = 0; $i < $node->childNodes->length; $i++) {
            if(XML_ELEMENT_NODE == $node->nodeType) {
                $this->_fillLinks($node->childNodes->item($i));
            }
        }
        
        return true;
    }
    
    /**
     * 
     * @param string $selector
     */
    protected function _proceedSimpleSelector($selector) {
        if(in_array($selector, $this->_uniques)) {
            unset($this->_uniques[array_search($selector, $this->_uniques)]);
        }
    }
    
    /**
     * 
     * @param DOMNode $node
     * @return boolean
     */
    protected function _proceedSimpleSelectors($node) {
        if(!$node->attributes) {
            return false;
        }
        
        $nodeName = $node->nodeName;
        
        $this->_proceedSimpleSelector($nodeName);
        
        $classAttribute = $node->attributes->getNamedItem('class');
        
        if($this->_maxLevel && count(explode(' ', $classAttribute)) > $this->_maxLevel) {
            return false;
        }
        
        if($classAttribute) {
            $class = $classAttribute->value;
            $this->_proceedSimpleSelector('.' . $class);
            $this->_proceedSimpleSelector($nodeName . '.' . $class);

        }

        $idAttribute = $node->attributes->getNamedItem('id');

        if($idAttribute) {
            $id = $idAttribute->value;
            $this->_proceedSimpleSelector('#' . $id);
            $this->_proceedSimpleSelector($nodeName . '#' . $id);
        }
    }
    
    /**
     * 
     * @param DOMNode $node
     * @return boolean
     */
    protected function _scanNode($node) {
        $this->_proceedSimpleSelectors($node);
        
        if(!$node->childNodes) {
            return false;
        }
        
        for($i = 0; $i < $node->childNodes->length; $i++) {
            if(XML_ELEMENT_NODE == $node->nodeType) {
                $this->_scanNode($node->childNodes->item($i));
            }
        }
        
        return true;
    }
    
    /**
     * 
     * @param DOMNode $path
     * @return string
     */
    protected function _css2xpath($path) {
        $path = (string) $path;
        if (strstr($path, ',')) {
            $paths       = explode(',', $path);
            $expressions = array();
            foreach ($paths as $path) {
                $xpath = $this->_transform(trim($path));
                if (is_string($xpath)) {
                    $expressions[] = $xpath;
                } elseif (is_array($xpath)) {
                    $expressions = array_merge($expressions, $xpath);
                }
            }
            return implode('|', $expressions);
        }

        $paths    = array('//');
        $path     = preg_replace('|\s+>\s+|', '>', $path);
        $segments = preg_split('/\s+/', $path);
        foreach ($segments as $key => $segment) {
            $pathSegment = $this->_tokenize($segment);
            if (0 == $key) {
                if (0 === strpos($pathSegment, '[contains(')) {
                    $paths[0] .= '*' . ltrim($pathSegment, '*');
                } else {
                    $paths[0] .= $pathSegment;
                }
                continue;
            }
            if (0 === strpos($pathSegment, '[contains(')) {
                foreach ($paths as $key => $xpath) {
                    $paths[$key] .= '//*' . ltrim($pathSegment, '*');
                    $paths[]      = $xpath . $pathSegment;
                }
            } else {
                foreach ($paths as $key => $xpath) {
                    $paths[$key] .= '//' . $pathSegment;
                }
            }
        }

        if (1 == count($paths)) {
            return $paths[0];
        }
        return implode('|', $paths);
    }

    /**
     * Tokenize CSS expressions to XPath
     *
     * @param  string $expression
     * @return string
     */
    protected function _tokenize($expression) {
        // Child selectors
        $expression = str_replace('>', '/', $expression);

        // IDs
        $expression = preg_replace('|#([a-z][a-z0-9_-]*)|i', '[@id=\'$1\']', $expression);
        $expression = preg_replace('|(?<![a-z0-9_-])(\[@id=)|i', '*$1', $expression);

        // arbitrary attribute strict equality
        $expression = preg_replace_callback(
            '|\[([a-z0-9_-]+)=[\'"]([^\'"]+)[\'"]\]|i',
            array($this, '_createEqualityExpression'),
            $expression
        );

        // arbitrary attribute contains full word
        $expression = preg_replace_callback(
            '|\[([a-z0-9_-]+)~=[\'"]([^\'"]+)[\'"]\]|i',
            array($this, '_normalizeSpaceAttribute'),
            $expression
        );

        // arbitrary attribute contains specified content
        $expression = preg_replace_callback(
            '|\[([a-z0-9_-]+)\*=[\'"]([^\'"]+)[\'"]\]|i',
            array($this, '_createContainsExpression'),
            $expression
        );

        // Classes
        $expression = preg_replace(
            '|\.([a-z][a-z0-9_-]*)|i',
            "[contains(concat(' ', normalize-space(@class), ' '), ' \$1 ')]",
            $expression
        );

        /** ZF-9764 -- remove double asterix */
        $expression = str_replace('**', '*', $expression);

        return $expression;
    }
    
    /**
     * 
     * @param array $matches
     * @return string
     */
    protected function _createEqualityExpression($matches) {
        return '[@' . strtolower($matches[1]) . "='" . $matches[2] . "']";
    }
    
    /**
     * 
     * @param array $matches
     * @return string
     */
    protected function _createContainsExpression($matches) {
        return "[contains(@" . strtolower($matches[1]) . ", '"
             . $matches[2] . "')]";
    }
    
    /**
     * 
     * @param array $matches
     * @return string
     */
    protected function _normalizeSpaceAttribute($matches) {
        return "[contains(concat(' ', normalize-space(@" . strtolower($matches[1]) . "), ' '), ' "
             . $matches[2] . " ')]";
    }
    
    /**
     * Entry
     */
    public function run() {
        $doc = new DOMDocument();
        $string = file_get_contents($this->_url);
        @$doc->loadHtml($string);
        
        for($i = 0; $i < $doc->childNodes->length; $i++) {
            $node = $doc->childNodes->item($i);

            if(XML_ELEMENT_NODE == $node->nodeType) {
                $this->_fillLinks($node);
            }
        }
        
        foreach($this->_paths as $key => $path) {
            echo $path . PHP_EOL;
            $doc = new DOMDocument();
        
            $tic = microtime(true);
            
            $string = file_get_contents($this->_scheme . '://' . $this->_host . $path);
            
            $tac = microtime(true);
            $this->_timeForLoad += ($tac - $tic);

            @$doc->loadHtml($string);

            $headNodeList = $doc->getElementsByTagName('head');
            $head = $headNodeList->item(0);
            for($i = 0; $i < $head->childNodes->length; $i++) {
                $node = $head->childNodes->item($i);
                if(XML_ELEMENT_NODE == $node->nodeType) {
                    $typeAttribute = $node->attributes->getNamedItem('type');
                    $type = '';
                    if($typeAttribute) {
                        $type = $typeAttribute->value;
                    }

                    $relAttribute = $node->attributes->getNamedItem('rel');
                    $rel = '';
                    if($relAttribute) {
                        $rel = $relAttribute->value;
                    }

                    if('link' == $node->nodeName && ('text/css' == $type || 'stylesheet' == $rel)) {
                        $hrefAttribute = $node->attributes->getNamedItem('href');
                        $href = '';
                        if($hrefAttribute) {
                            $href = $hrefAttribute->value;
                        }

                        if(false !== strpos($href, '.css')) {
                            if(false === strpos($href, $this->_scheme . '://')) {
                                $href = $this->_scheme . '://' . $this->_host . $href;
                            }
                            if(!in_array($href, $this->_cssFiles)) {
                                array_push($this->_cssFiles, $href);
                                $this->_fillUniques($href);
                            }
                        }
                    }
                }
            }

            for($i = 0; $i < $doc->childNodes->length; $i++) {
                $node = $doc->childNodes->item($i);

                if(XML_ELEMENT_NODE == $node->nodeType) {
                    $this->_scanNode($node);
                }
            }

            $domXpath = new DOMXPath($doc);

            foreach($this->_uniques as $key => $selector) {
                $depth = count(explode(' ', $selector));
                
                if($this->_maxLevel && $depth > $this->_maxLevel) {
                    array_push($this->_depthExceededSelectors, $selector);
                }
                
                $xpath = $this->_css2xpath($selector);
                $nodeList = $domXpath->query($xpath);
                if($nodeList->length != 0) {
                    unset($this->_uniques[$key]);
                }
            }
            
            if($this->_maxPageCount && $key > $this->_maxPageCount) {
                break;
            }
        }
        
        $this->_getReport();
    }
    
    /**
     * Generates report
     */
    protected function _getReport() {
        echo PHP_EOL;
        
        foreach($this->_cssFiles as $file) {
            $lines = array();
            foreach(file('/tmp/' . md5($file)) as $key => $line) {
                foreach($this->_uniques as $selector) {
                    if(false !== strpos($line, $selector)) {
                        if(!array_key_exists($key, $lines)) {
                            $lines[$key] = trim(substr($line, 0, strpos($line, '{')));
                        }
                    }
                }
            }
            
            foreach($lines as $key => $selector) {
                echo $file . ':' . ($key + 1) . ' ' . $selector . PHP_EOL;
            }
            if(count($lines)) {
                echo 'total lines in file: ' . count($lines) . PHP_EOL;
            }
            echo PHP_EOL;
        }
    }
}