#!/usr/bin/env php

<?php
$tic = microtime(true);

require_once './CssAnalyzer.php';

$options = getopt("h:a:l:p:");

$scanAllSite = isset($options['a']) ? (int) $options['a'] : false;
$maxLevel = isset($options['l']) ? (int) $options['l'] : 0;
$maxPageCount = isset($options['p']) ? (int) $options['p'] : 0;

$analyzer = new CssAnalyzer($options['h'], $scanAllSite, $maxLevel, $maxPageCount);
$analyzer->run();


$tac = microtime(true);

$timeDiff = $tac - $tic - $analyzer->getTimeForLoad();

echo "time:\t\t" . $timeDiff . PHP_EOL;
echo "time for load:\t" . $analyzer->getTimeForLoad() . PHP_EOL;

echo "memory peak:\t" . memory_get_peak_usage() . PHP_EOL;

echo "memory:\t\t" . memory_get_usage() . PHP_EOL;